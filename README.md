# Proiect TW

With Computer Science being a relatively new domain of research you will likely found everything you want to read online: research papers, news, articles, source code and even more. The Internet is, generally, a great tool for reading and researching about what you're interested in but you will run into some problems: the information you find may not be reliable, there are so many places to look and so much information to be found you will not know what to do and, in a sea of information, how do you find only what interests you.
The Topic-based Resource eXplorer(or TRex for short) is here to solve the problem. TReX collects what you need from reliable sources like Feedly, GitHub, Google Scholar, Lanyrd, Pocket, Slideshare or Vimeo(by using the API provided by these sources, sorts it using a tagging system allowing you to follow what you're interested in and brings it all you in a easy to find place.

Team:
- Valeriu Moise
- Alexandru Barbu
- Cosmin Lupu
- Catalin Belu